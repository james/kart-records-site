@extends('layouts.admin')

@section('content')
<h1>{{ $course->exists ? 'Edit Course' : 'New Course' }}</h1>

<form action="{{ action('Admin\CourseController@' . ($course->exists ? 'update' : 'store'), compact('course')) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	@if($course->exists)
		<input type="hidden" name="_method" value="PUT" />
	@endif
	
	<label>
		Name:
		<input type="text" name="name" value="{{ $course->name }}" />
	</label>
	
	<label>
		Add Map:
		<select name="add_map">
			<option value="">-- Select --</option>
			@foreach($freeMaps as $map)
				<option value="{{ $map->getKey() }}">{{ $map->map_tag }} - {{ $map->checksum }}</option>
			@endforeach
		</select>
	</label>
	
	@if($course->exists)
		Remove Maps:
		@foreach($course->maps as $map)
			<label>
				<input type="checkbox" name="remove_map[]" value="{{ $map->getKey() }}" />
				{{ $map->map_tag }} - {{ $map->checksum }}
			</label>
		@endforeach
	@endif
	
	<input type="submit" value="Save" />
</form>

@if($course->exists)
	<form action="{{ action('Admin\CourseController@destroy', compact('course')) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="DELETE" />
		<input type="submit" value="Delete" onclick="return confirm('Are you sure you want to delete this course?')" />
	</form>
@endif
@stop
